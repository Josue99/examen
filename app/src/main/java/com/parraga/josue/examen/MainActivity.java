package com.parraga.josue.examen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText Cedula, Nombres, Apellidos, FechaNacimiento, Facultad;
    Button Ingreso, Consulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cedula = (EditText) findViewById(R.id.txtCedula);
        Nombres = (EditText) findViewById(R.id.txtNombres);
        Apellidos = (EditText) findViewById(R.id.txtApellidos);
        FechaNacimiento = (EditText) findViewById(R.id.txtFechaNacimiento);
        Facultad = (EditText) findViewById(R.id.txtFacultad);

        Ingreso = (Button) findViewById(R.id.btnIngreso);
        Consulta = (Button) findViewById(R.id.btnConsulta);

        final DeveloperuBD developeruBD = new DeveloperuBD(getApplicationContext());

        Ingreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                developeruBD.agregarCursos(Cedula.getText().toString(), Nombres.getText().toString(), Apellidos.getText().toString(), FechaNacimiento.getText().toString(), Facultad.getText().toString());
                Toast.makeText(getApplicationContext(), "SE AGREGO CORRECTAMENTE", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
