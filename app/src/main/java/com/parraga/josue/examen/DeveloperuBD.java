package com.parraga.josue.examen;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DeveloperuBD extends SQLiteOpenHelper {

    private static final String NOMBRE_BD="developeru.bd";
    private static final int VERSION_BD=1;
    private static final String TABLA_CURSOS="CREATE TABLE CURSOS(CEDULA TEXT, NOMBRES TEXT, APELLIDOS TEXT, FECHANACIMIENTO TEXT, FACULTAD TEXT)";

    public DeveloperuBD(Context context) {
        super(context, NOMBRE_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_CURSOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLA_CURSOS);
        db.execSQL(TABLA_CURSOS);
    }

    public void agregarCursos(String cedula, String nombres, String apellidos, String fechaNacimiento, String facultad){
        SQLiteDatabase bd=getWritableDatabase();
        if(bd!=null){
            bd.execSQL("INSERT INTO DATOS VALUES('"+cedula+"', '"+nombres+"', '"+apellidos+"', '"+fechaNacimiento+"', '"+facultad+"')");
            bd.close();
        }
    }
}
